[![Download](https://img.shields.io/badge/Download-latest-brightgreen.svg?style=flat-square)](https://git.mosad.xyz/Seil0/ActionPi/releases)
![Latest](https://img.shields.io/badge/Release-1.02-blue.svg?style=flat-square)
[![License: GPL v3](https://img.shields.io/badge/License-GPL%20v3-blue.svg?style=flat-square)](https://www.gnu.org/licenses/gpl-3.0)

# ActionPi

ActionPi is an easy to use Action Camera for the Raspberry Pi. The full documentation can be found in our [wiki](https://git.mosad.xyz/Seil0/ActionPi/wiki).

Used Librarys:     
- http://www.uco.es/investiga/grupos/ava/node/40   
- http://www.airspayce.com/mikem/bcm2835/

## Installation

to build with geany add this to Build Commands:    
-  `g++ -Wall -c "%f" -l raspicam -l bcm2835 -std=c++0x`   
- if the first command doesn't work `g++ -Wall -o"%e" "%f" -l raspicam -l bcm2835 -sdt=c++14`

ActionPi © 2016-2019 Kellerkinder [www.mosad.xyz](https://www.mosad.xyz)
